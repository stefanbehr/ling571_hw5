#!/bin/env python2.7

"""
This is the test script for Homework 5 (ling571, Winter 2013).
author: Julie Medero
modified 2013: Julie Medero
Usage: hw5_checker.py --key hw5.key --hyp hw5.out
"""

import nltk
import sys, re, argparse
from itertools import takewhile

digitRE = re.compile("\d+$")

def parseLine(line, lp):
    f=lp.parse(line)
    variableBindings={}

    for var in list(f.variables()):
        newVar=nltk.sem.Variable(digitRE.sub("", var.name))
        if newVar <> var: 
            variableBindings[var] = nltk.sem.Variable(digitRE.sub("", var.name))

    return f.substitute_bindings(variableBindings)

def isNotComment(line): return not(line.startswith("#"))

def checkLine(hypLine, fp, lp):
    keyParses = [parseLine(x.strip(), lp) for x in takewhile(isNotComment, fp) if x.strip()]

    try:
        hypParse=parseLine(hypLine, lp)
        equivalentParses = [x.equiv(hypParse) for x in keyParses]
    except Exception, e:
        print >> sys.stderr, "FAILED to parse hypothesis line", hypLine, ":", e
        return False

    return str(True in equivalentParses)


def main(opts):
    lp=nltk.sem.LogicParser()

    list(takewhile(lambda x: not(x.startswith("#")), opts.keyFile)) # Read off the top comment line in keyFile
    
    for line in opts.hypFile:
        if line.startswith("#"): continue
        print checkLine(line.strip(), opts.keyFile, lp)

    opts.keyFile.close()
    opts.hypFile.close()

if __name__=="__main__":
    parser = argparse.ArgumentParser()
    parser.add_argument("-k","--key",dest="keyFile", metavar="FILE", help="Evaluate against the key file FILE", required=True, type=argparse.FileType('r'))
    parser.add_argument("-y", "--hyp", dest="hypFile", metavar="FILE", help="Evaluate the hypothesis analyses in FILE", required=True, type=argparse.FileType('r'))

    opts = parser.parse_args()

    main(opts)
