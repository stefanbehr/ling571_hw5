#!/usr/bin/env python2.7

# Stefan Behr
# LING 571
# Homework 5

import argparse, nltk

def main(**kwargs):
	grammar = nltk.load_parser("file:{0}".format(kwargs["grammar_path"])).grammar()
	parser = nltk.parse.FeatureEarleyChartParser(grammar)
	head = kwargs["head"]

	with open(kwargs["sentence_path"]) as sentence_file:
		with open(kwargs["output_path"], "w") as output_file:
			for sentence in sentence_file:
				if head == 0:
					break
				head -= 1
				sentence = sentence.strip()
				parses = parser.nbest_parse(nltk.wordpunct_tokenize(sentence))
				if parses:
					print >> output_file, parses[0].node["SEM"]
					print parses[0].node["SEM"]
				else:
					print >> output_file, ""

if __name__ == "__main__":
	argparser = argparse.ArgumentParser(description="Script for parsing sentences with semantic grammar.")
	argparser.add_argument("-g", dest="grammar_path", default="semantic.fcfg", action="store", help="Path to grammar.")
	argparser.add_argument("-s", dest="sentence_path", default="hw5_sentences.txt", action="store", help="Path to sentences to be parsed.")
	argparser.add_argument("-o", dest="output_path", default="hw5.out", action="store", help="Output path.")
	argparser.add_argument("-n", dest="head", default=-1, type=int, action="store", help="Limit on sentences processed.")
	opts = argparser.parse_args()

	main(**opts.__dict__)
