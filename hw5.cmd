# Stefan Behr
# LING 571
# Homework 5

Executable = /opt/python-2.7/bin/python2.7
Universe   = vanilla
getenv     = true
input      = 
output     = 
error      = hw5.error
Log        = hw5.log
arguments  = parse.py -s hw5_sentences.txt
transfer_executable = false
request_memory = 2*1024
Queue